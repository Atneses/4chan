#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division

import datetime
import json
import os
import random
import re
import shutil
import sys
import time
import urllib

from basc_py4chan import Board
from tqdm import tqdm
from unidecode import unidecode

from data import Database


SAVE_FOLDER = 'downloads/'
FINISHED_FOLDER = 'finished/'


class FourChanDownloader(object):
    sleep_time = 50
    sleep_time_image = 5
    max_rand = 3
    progress_bar = 25
    WINDOWS_INVALID_CHARS = r'[\*,<,>,:,\",/,\\,|,?]'
    LIST_JSON = 'list.json'
    DATE_STRING = '%Y.%m.%d %H.%M.%S.%f'

    def __init__(self, sleep_time=20):
        self.sleep_time = sleep_time
        self.db = Database()

    def create_finish_folder(self, folder):
        folder = str(folder)
        folder = re.sub(self.WINDOWS_INVALID_CHARS, '', folder)
        if not os.path.exists(FINISHED_FOLDER + folder):
            os.makedirs(FINISHED_FOLDER + folder)
        return FINISHED_FOLDER + folder

    def create_folder(self, folder):
        # < >: " / \ | ? *
        folder = str(folder)
        folder = re.sub(self.WINDOWS_INVALID_CHARS, '', folder)
        if not os.path.exists(SAVE_FOLDER + folder):
            os.makedirs(SAVE_FOLDER + folder)

    def remove_accented_chars(self, string):
        return unidecode(string)

    def clean_string(self, string):
        string = string.replace('.', ' ', string.count('.') - 1)
        name = string.split('.')[0]
        ext = string.split('.')[1]
        ext = ext.lower()

        name = re.sub(r'[&#;?*]+', '', name)

        name = name.replace(':', '-')
        name = re.sub(r'[/_-]+', ' ', name)

        name = name.title()
        name = self.remove_accented_chars(name)
        string = '.'.join([name, ext])
        return string

    def clean_thread_name(self, name):
        name = name.replace('_', ' ')
        name = name.replace('-', ' ')
        name = name.replace('/', ' ')
        name = name.replace('?', ' ')
        name = name.replace('#', '')
        name = name.replace('&', '')
        name = name.replace('...', '')
        name = name.replace(';', '')
        name = name.replace(u'\u2019', ' ')
        name = name.strip()
        name = name.title()
        name = self.remove_accented_chars(name)
        name = re.sub(self.WINDOWS_INVALID_CHARS, '', name)
        return name

    def download_board(self, thread, thread_name):
        x = sum([1 for x in thread.file_objects()])

        for file_object in tqdm(thread.file_objects(), total=x, ncols=0):
            file0 = file_object.__dict__['_post'].file
            file1 = file_object.__dict__['_post'].file

            file_md5_hex = file0.file_md5_hex
            file_url = file0.file_url
            filename = file0.filename
            filename_original = file0.filename_original
            name = filename_original if filename_original else filename
            name = self.clean_string(name)

            if not self.db.check_md5_database(file_md5_hex):
                if self.is_file_exist(SAVE_FOLDER + thread_name + '/' + name):
                    split = name.split('.')
                    name = split[0] + ' ' + datetime.datetime.now().strftime(
                        self.DATE_STRING) + '.' + split[1]

                print(f'{thread_name} - Download: {name}')

                try:
                    self.download_image(file_url, thread_name, name)
                    self.sleep_time_image = random.randrange(1, self.max_rand)
                    print(f'Waiting {self.sleep_time_image} seconds')
                    time.sleep(self.sleep_time_image)

                    # Save MD5 in database
                    self.db.insert_md5(file_md5_hex, name, SAVE_FOLDER + thread_name + '/')
                except IOError as error:
                    print(f'DOWNLOAD BOARD {error}')
            else:
                if not self.db.check_filename(filename):
                    print(f'Inserted new name: {filename}')
                    self.db.insert_md5(file_md5_hex, filename)

    def download_image(self, url, folder_name, name):
        try:
            urllib.request.urlretrieve(url, SAVE_FOLDER + folder_name + '/' + name, self.download_image_percentage)
        except IOError as ex:
            print(f'Error downloading file: {ex}')
        except ZeroDivisionError:
            print('Division by zero.')

    def download_image_percentage(self, block_count, block_size, total_size):
        if block_count == 0:
            current = block_size
        else:
            current = block_count * block_size
        percentage = (current * 100) / total_size
        if percentage > 100:
            percentage = 100
        bar = int((self.progress_bar * percentage) / 100)
        kb = total_size / 1024
        mb = kb / 1024
        kb_str = '%.2f Kb' % kb
        mb_str = '%.2f Mb' % mb

        print('\r'),
        print(kb_str if mb < 1 else mb_str), ('=' * bar),
        sys.stdout.flush()  # flush is needed.

    def move_finished_thread(self, name):
        name = str(name)
        route = SAVE_FOLDER + name + '/'
        files = os.listdir(route)
        self.create_finish_folder(name)

        try:
            for f in files:
                shutil.move(SAVE_FOLDER + name + '/' + f, FINISHED_FOLDER + name + '/' + f)
        except (OSError, IOError) as os_error:
            print(str(os_error))

        try:
            os.rmdir(SAVE_FOLDER + name)
        except (WindowsError, IOError) as windows_error:
            print(str(windows_error))

    def is_file_exist(self, path):
        return os.path.isfile(path)

    def open_thread_list(self):
        with open(self.LIST_JSON) as json_file:
            thread_list = json.load(json_file)
            json_file.close()
        return thread_list

    def remove_thread_from_list(self, ids):
        thread_list = list(self.open_thread_list())
        index = next(index for (index, d) in enumerate(thread_list) if d["id"] == ids)
        thread_list.pop(index)

        with open(self.LIST_JSON, 'w') as outfile:
            json.dump(thread_list, outfile, indent=4)
            outfile.close()

    def scan_boards(self):
        try:
            while True:
                for data in self.open_thread_list():
                    board = Board(data['board'])

                    if board.thread_exists(data['id']):
                        thread = board.get_thread(data['id'])
                        thread.update()
                        thread_name = thread.topic.subject

                        if not thread_name:
                            thread_name = thread.semantic_slug

                        thread_name = self.clean_thread_name(thread_name)
                        self.create_folder(thread_name)
                        print(f'{thread_name} {data["id"]}')

                        if not thread.archived and not thread.is_404:
                            self.download_board(thread, thread_name)

                        elif thread.archived or thread.bumplimit:
                            print(f'{thread_name} is archived, will be deleted after download images.')
                            self.download_board(thread, thread_name)
                            self.move_finished_thread(thread_name)
                            self.remove_thread_from_list(thread.id)
                        else:
                            print(f'{thread_name} is 404, will be removed from list')
                            self.move_finished_thread(thread_name)
                            self.remove_thread_from_list(thread.id)
                    else:
                        print(f'Thread {data["id"]} does not exist removing fromm list')
                        self.remove_thread_from_list(data['id'])

                print(f'Waiting {self.sleep_time} seconds')
                time.sleep(self.sleep_time)
        except KeyboardInterrupt:
            pass

    def search_keyword(self):
        print('Searching for keyword...')
        with open('keywords.json') as keywords_file:
            keywords = json.load(keywords_file)
            thread_list = list(self.open_thread_list())
            for b in keywords['boards']:
                board = Board(b)
                threads = board.get_all_threads()
                for t in threads:
                    for k in keywords['words']:
                        subject = str(t.topic.subject)
                        if t.op.text_comment.lower().find(k) > 0 or subject.lower().find(k) > 0:
                            # Save to JSON file
                            thread_list.append({
                                'id': t.id,
                                'board': b
                            })
            thread_list = [dict(t) for t in {tuple(d.items()) for d in thread_list}]
            # Save new list to JSON file
            with open(self.LIST_JSON, 'w') as outfile:
                json.dump(thread_list, outfile, indent=4)
                outfile.close()
