# -*- coding: utf-8 -*-
import datetime
import hashlib
import multiprocessing
import os
import sqlite3
import time

from joblib import Parallel, delayed
from tqdm import tqdm

from data import Database

PATH = 'D:\\Aza\\Descargas\\.Anime'


def get_file_md5(path, filename):
    hash_md5 = hashlib.md5()
    with open(path, 'rb') as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    hash = hash_md5.hexdigest()
    # print (filename), (hash)
    return hash, filename, path, str(datetime.datetime.today())


def get_file_list():
    files_list = []
    for root, dirs, files in os.walk(unicode(PATH)):
        for f in files:
            file_dir = os.path.join(root, f)
            # .jpeg .png .gif .webm  .jpg
            if file_dir.lower().endswith('.jpg'):
                files_list.append([file_dir, f])
    return files_list


if __name__ == '__main__':
    start_time = time.time()
    files = get_file_list()
    print(len(files))
    conn = sqlite3.connect('4chan.db')
    num_cores = multiprocessing.cpu_count()
    print(f'Cores to use: {num_cores}')
    results = Parallel(n_jobs=num_cores)(
        delayed(get_file_md5)(f[0], f[1]) for f in tqdm(files))
    print(results)
    print(f'Elapsed time: {(time.time() - start_time)} seconds')

    db = Database()
    db.insert_many_md5(results)
