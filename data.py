# -*- coding: utf-8 -*-
import datetime
import sqlite3


class Database(object):
    def __init__(self):
        self.md5_table = 'md5'
        self.md5_column = 'md5'
        self.filename_column = 'filename'
        self.file_path = 'file_path'
        self.date_column = 'date'
        self.database_name = '4chan.db'
        self.conn = sqlite3.connect(self.database_name)
        query = f'''CREATE TABLE IF NOT EXISTS {self.md5_table} ({self.md5_column} TEXT, {self.filename_column} TEXT, {self.file_path} TEXT NULL, {self.date_column} TEXT)'''
        self.conn.execute(query)

    def __del__(self):
        self.conn.close()

    def insert_md5(self, md5, filename, file_path=None):
        md5_date = str(datetime.datetime.today())
        query_insert = f'''INSERT INTO {self.md5_table} VALUES (?, ?, ?, ?)'''
        if not file_path:
            file_path = u''
        try:
            self.conn.execute(query_insert, (md5, filename, file_path + filename, md5_date))
            self.conn.commit()
        except sqlite3.IntegrityError as error:
            print(error)
        except UnicodeEncodeError as error:
            print(error)

    def insert_many_md5(self, files):
        query_insert = f'''INSERT INTO {self.md5_table} VALUES(?, ?, ?, ?)'''
        self.conn.executemany(query_insert, files)
        self.conn.commit()

    def check_md5_database(self, md5):
        query = f'''SELECT * FROM {self.md5_table} WHERE {self.md5_column} LIKE '{md5}' '''
        list_md5 = self.conn.execute(query).fetchall()

        if len(list_md5) > 0:
            return True
        return False

    def check_filename(self, filename):
        query = f'''SELECT * FROM {self.md5_table} WHERE {self.filename_column} LIKE '{filename}' COLLATE NOCASE'''
        list_files = self.conn.execute(query).fetchall()

        if len(list_files) > 0:
            return True
        return False
