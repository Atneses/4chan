#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from chan import FourChanDownloader

if __name__ == '__main__':
    print(sys.argv)
    chan = FourChanDownloader()
    chan.search_keyword()
    chan.scan_boards()
